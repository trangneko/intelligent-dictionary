from django import forms
from .models import UserProfile
 
class IDict(forms.Form):
    sentence = forms.CharField()
    word = forms.CharField()
    response_language = forms.CharField()
    CHOICES = (('1', 1),('2', 2),('3', 3),('4', 4),('5', 5),)
    number_of_meanings = forms.ChoiceField(choices=CHOICES)

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['bio', 'avatar']