from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import IDict
from .forms import UserProfileForm, UserProfile
import openai
import json

openai.api_key = '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'


def get_completion(prompt, model="gpt-3.5-turbo",temperature=0):
    messages = [{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature, # this is the degree of randomness of the model's output
    )
    return response.choices[0].message["content"]
def get_response(sentence, word, number_of_meanings=3, response_language="English"):
    prompt = f"""
        You are an intelligent dictionary.
        Given a sentence and a word in that sentence, you need to:

        1. Identify the language of the sentence.
        2. Provide up to {number_of_meanings} closest meanings in {response_language} to the meaning of the word in the sentence, along with their similarity scores.
        Include sample sentences in the identified language and their corresponding meanings in {response_language}.
        Ensure that the returned meanings are in {response_language}, and limit the sample sentences to a maximum of 20 words.

        3. Sort the meanings in descending order based on the similarity score.
        4. Format your response in JSON as follows:

        {{
            "Language": "",
            "Meanings": [
                {{
                    "meaning": "",
                    "similarity_score": "",
                    "example_sentence": "",
                    "example_sentence_meaning": ""
                }},
                ...
            ]
        }}
        
        The sentence is enclosed within triple backticks, and the word is enclosed within triple backticks.
        Sentence: ```{sentence}```
        Word: ```{word}```
    """

    response = get_completion(prompt, temperature=0.5)
    # print(response)
    return json.loads(response)

def search(request):
    if request.method == 'POST':
        form = IDict(request.POST)
        # print(form)
        if form.is_valid():
            # Process the form data
            # Access the form fields using form.cleaned_data dictionary
            sentence = form.cleaned_data['sentence']
            word = form.cleaned_data['word']
            response_language = form.cleaned_data['response_language']
            number_of_meanings = form.cleaned_data['number_of_meanings']
            
            # print(word, sentence, number_of_meanings, response_language)
            
            response = get_response(sentence, word, number_of_meanings, response_language)
            print(response, type(response))
            # Redirect to a success page or do further processing
            return render(request, 'search.html', {'form': form, 'response': response})
        else:
            print(form.errors.as_data())
    else:
        form = IDict()
    return render(request, 'search.html', {'form': form})

def index(request):
    return render(request, 'home.html')

def contact(request):
    return render(request, 'contact.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        print (form)
        if form.is_valid():
            user = form.save()
            # Create UserProfile for the newly registered user
            UserProfile.objects.create(user=user)
            # Log the user in
            login(request, user)
            return redirect('login')  # Redirect to the homepage or any other desired page
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})

@login_required
def profile(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=request.user.userprofile)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = UserProfileForm(instance=request.user.userprofile)
    return render(request, 'profile.html', {'form': form})

# @login_required
# def custom_login(request, *args, **kwargs):
#     # If user is already authenticated, redirect to their profile
#     print(1231231)
#     if request.user.is_authenticated:
#         return redirect('profile')

#     # If user is not authenticated, continue with the regular login view
#     return auth_login(request, *args, **kwargs)