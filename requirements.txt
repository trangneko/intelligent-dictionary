dj-database-url==1.0.0
Django==3.2.20
django-heroku==0.3.1
django-storages==1.13.1
openai==0.27.8
whitenoise==6.2.0
